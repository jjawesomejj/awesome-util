package ListHelper

import (
	"math"
)

/**
获取list 里面的map的某个键的值
*/
func GetColumn(list []map[string]interface{}, key string) []interface{} {
	res := make([]interface{}, 0)
	for _, value := range list {
		res = append(res, value[key])
	}
	return res
}

// ListChunk 对list 进行切片
func ListChunk(list []interface{}, batchSize int) [][]interface{} {
	chunkNum := int(math.Ceil(float64(len(list)) / float64(batchSize)))
	if chunkNum <= 1 {
		return [][]interface{}{list}
	} else {
		res := make([][]interface{}, chunkNum)
		for i := range res {
			res[i] = make([]interface{}, batchSize)
		}
		for index, item := range list {
			bigIndex := math.Ceil(float64(index)/float64(batchSize)) - 1
			if bigIndex <= 0 {
				bigIndex = 0
			}
			smallIndex := index % batchSize
			res[int(bigIndex)][smallIndex] = item
		}
		return res
	}
}

package commonhelper

import (
	"crypto/md5"
	"fmt"
	"strconv"
	"strings"
	"time"
)
import "math/rand"

var seed int64 = time.Now().UnixNano()
var index int64 = 0

func getSeed() int64 {
	index = index + 1
	seed = time.Now().UnixNano() + index
	return seed
}
func Format(str string, params ...string) string {
	for _, char := range params {
		str = strings.Replace(str, "{}", char, 1)
	}
	return str
}

func FormatJson(str string, args map[string]string) string {
	for key, value := range args {
		str = strings.ReplaceAll(str, "{"+key+"}", value)
	}
	return str
}
func Rand(num int) string {
	str := ""
	rand.Seed(getSeed())
	for i := 0; i < num; i++ {
		str = str + strconv.Itoa(rand.Intn(10))
	}
	return str
}
func RandInt(min int, max int) int {
	rand.Seed(getSeed())
	return rand.Intn(max) - min
}
func Md5(str string) string {
	//方法一
	data := []byte(str)
	has := md5.Sum(data)
	md5str1 := fmt.Sprintf("%x", has) //将[]byte转成16进制
	return md5str1
}

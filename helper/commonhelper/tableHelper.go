package commonhelper

import (
	"fmt"
	"github.com/liushuochen/gotable"
)

func PrintTable(values []map[string]string, isPrint bool) (string, error) {
	keys := make([]string, 0)
	for key, _ := range values[0] {
		keys = append(keys, key)
	}
	table, err := gotable.Create(keys...)
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}
	for _, value := range values {
		err := table.AddRow(value)
		if err != nil {
			fmt.Println(err.Error())
			return "", err
		}
	}
	res := table.String()
	if isPrint {
		fmt.Println(res)
	}
	return res, err
}

package commonhelper

import (
	"errors"
	"io/fs"
	"io/ioutil"
	"os"
	path2 "path"
	"path/filepath"
	"strings"
)

/*
*
用字符串的方式读取文件
*/
func ReadFileAsString(path string) string {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		panic(errors.New(err.Error()))
	}
	str := string(b)
	return str
}

/*
*
用byte的方式读取文件
*/
func ReadFileAsBytes(path string) []byte {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err.Error())
	}
	return b
}

/*
*
判断文件是否存在
*/
func CheckFileIsExist(filename string) bool {
	var exist = true
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		exist = false
	}
	return exist
}

/*
*
写文件
*/
func WriteFile(filename string, content string, priv fs.FileMode) error {
	Mkdir(path2.Dir(filename), priv)
	return ioutil.WriteFile(filename, []byte(content), priv)
}
func WriteFileByStream(filename string, content []byte, priv fs.FileMode) {
	Mkdir(path2.Dir(filename), priv)
	ioutil.WriteFile(filename, content, priv)
}

/*
*
判断文件夹是否存在
*/
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

/*
*
创建文件夹
*/
func Mkdir(filePath string, mode fs.FileMode) {
	exist, err := PathExists(filePath)
	if err != nil {
		panic(err)
	}
	if !exist {
		err := os.MkdirAll(filePath, os.ModePerm)
		if err != nil {
			panic(err.Error())
		}
	}
}

/*
*
追加数据到文件中
*/
func AppendFile(path string, content string) {
	Mkdir(filepath.Dir(path), 0777)
	fd, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		panic(err.Error())
	}
	_, err = fd.WriteString(content)
	if err != nil {
		panic(err.Error())
	}
	err = fd.Close()
	if err != nil {
		panic(err.Error())
	}
}

/*
获取程序运行路径
*/
func GetCurrentRunningPath() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err.Error())
	}
	return strings.Replace(dir, "\\", "/", -1)
}

module gitee.com/jjawesomejj/awesome-util

replace gitee.com/jjawesomejj/awesome-util => ../awesome-util

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/websocket v1.5.0
	github.com/liushuochen/gotable v0.0.0-20221119160816-1113793e7092
	go.uber.org/zap v1.27.0
	golang.org/x/crypto v0.1.0
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
	gorm.io/gorm v1.25.5
)

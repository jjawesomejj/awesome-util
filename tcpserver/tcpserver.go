package tcpserver

import (
	"fmt"
	"net"
	"strconv"
)

const OnStart string = "on_tcp_start"
const OnMessage string = "on_tcp_message"
const OnConnect string = "on_tcp_connect"
const OnDisConnect string = "on_tcp_disconnect"
const OnError string = "on_tcp_err"
const OnClose string = "on_tcp_close"

type TcpServer struct {
	listenPort int
	debug      bool
	eventMap   map[string]interface{}
	listener   *net.TCPListener
}

func initServer(server *TcpServer) {
	if server.eventMap == nil {
		server.eventMap = make(map[string]interface{})
	}
}

func (server *TcpServer) Start(port int, debug bool) {
	initServer(server)
	server.listenPort = port
	server.debug = debug
	var tcpAddr *net.TCPAddr
	tcpAddr, _ = net.ResolveTCPAddr("tcp", "0.0.0.0:"+strconv.Itoa(port))
	tcpListener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		if _, ok := server.eventMap[OnError]; ok {
			errFun := server.eventMap[OnError].(func(err2 error))
			errFun(err)
			if debug {
				fmt.Println("开启调试模式:", err)
			}
		}
		return
	}
	/**
	执行绑定的server start函数
	*/
	if _, ok := server.eventMap[OnStart]; ok {
		fun := server.eventMap[OnStart].(func())
		if debug {
			fmt.Println("开启调试模式:", "执行监听前的预处理")
		}
		go fun()
	}
	if debug {
		fmt.Println("开启调试模式:", "开始监听:"+strconv.Itoa(port))
	}
	server.listener = tcpListener
	for {
		tcpConn, err := tcpListener.AcceptTCP()
		if err != nil {
			continue
		}
		/**
		执行绑定的客户端连接到服务器端函数
		*/
		if debug {
			fmt.Println("开启调试模式:", "客户端连接到=>"+tcpConn.RemoteAddr().String())
		}
		if _, ok := server.eventMap[OnConnect]; ok {
			fun := server.eventMap[OnConnect].(func(connection net.Conn))
			go fun(tcpConn)
		}
		go server.handler(tcpConn)
	}
}
func (server *TcpServer) OnStart(fun func()) {
	initServer(server)
	server.eventMap[OnStart] = fun
}
func (server *TcpServer) OnMessage(fun func(connection net.Conn, message []byte)) {
	initServer(server)
	server.eventMap[OnMessage] = fun
}

func (server *TcpServer) OnConnect(fun func(connection net.Conn)) {
	initServer(server)
	server.eventMap[OnConnect] = fun
}

func (server *TcpServer) OnDisConnect(fun func(connection net.Conn)) {
	initServer(server)
	server.eventMap[OnDisConnect] = fun
}
func (server *TcpServer) OnError(fun func(err error)) {
	initServer(server)
	server.eventMap[OnError] = fun
}
func (server *TcpServer) OnClose(fun func(conn net.Conn)) {
	initServer(server)
	server.eventMap[OnClose] = fun
}
func (server *TcpServer) Close() {
	initServer(server)
	server.listener.Close()
}
func (server *TcpServer) handler(connection net.Conn) {
	initServer(server)
	for true {
		message, err := readByte(connection)
		if err != nil {
			if _, ok := server.eventMap[OnClose]; ok {
				closeFun := server.eventMap[OnClose].(func(conn net.Conn))
				if server.debug {
					fmt.Println("开启调试模式:", "客户端关闭连接=>"+connection.RemoteAddr().String())
				}
				go closeFun(connection)
			}
			break
		}
		if _, ok := server.eventMap[OnMessage]; ok {
			messageFun := server.eventMap[OnMessage].(func(connection net.Conn, message []byte))
			go messageFun(connection, message)
		}

	}
}
func readByte(connection net.Conn) ([]byte, error) {
	buffer := make([]byte, 512)
	n, err := connection.Read(buffer)
	//fmt.Println(buffer,"buffer")
	buffer = buffer[0:n]
	return buffer, err
}

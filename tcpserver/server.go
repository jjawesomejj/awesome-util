package tcpserver

import "net"

type absServer interface {
	Start(port int)
	OnStart(fun func())
	OnConnect(fun func(connection net.Conn))
	OnDisConnect(fun func(connection net.Conn))
	OnMessage(fun func(connection net.Conn, message string))
	OnError(fun func(err error))
	OnClose(fun func())
	Close(fun func(err error))
}

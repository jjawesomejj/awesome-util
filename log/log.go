package log

var logLevel = map[string]string{
	"all":   "all",
	"trace": "trace",
	"debug": "debug",
	"info":  "info",
	"warn":  "warn",
	"error": "error",
	"fatal": "fatal",
	"off":   "off",
}

type Log interface {
	All(message string)                    //最低等级日志
	Trace(message string)                  //低等级日志
	Debug(message string)                  //调试级别日志
	Info(message string)                   // 消息在粗粒度级别上突出强调应用程序的运行过程
	Warn(message string)                   //警告级别的日志
	Error(message string)                  //错误日志 程序继续运行
	Fatal(message string)                  //严重级别的日志
	Off(message string)                    //最高等级的日志
	writeLog(level string, message string) //写日志
}

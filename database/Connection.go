package database

import "gorm.io/gorm"

var dbConn *gorm.DB

func SetConnection(connection *gorm.DB) {
	dbConn = connection
}
func GetDbConnection() *gorm.DB {
	if dbConn == nil {
		panic("请先配置连接")
	}
	return dbConn
}

package main

import (
	"fmt"
	"gitee.com/jjawesomejj/awesome-util/config"
	"gitee.com/jjawesomejj/awesome-util/tcpClient"
	"net"
	"os"
	"strconv"
	"time"
)

func getCommandOrFailed(key string) interface{} {
	return config.GetConfigByKey(key, func() interface{} {
		fmt.Println("--" + key + " required")
		os.Exit(0)
		return ""
	})
}

func main() {
	//con, err := net.Dial("tcp", "12")
	//if err != nil {
	//	fmt.Println(err)
	//} else {
	//	fmt.Println(con.RemoteAddr())
	//}
	//return
	config.HandleCommandLineSetToConfig()
	command := getCommandOrFailed("c").(string)
	funcs := map[string]func(){}
	funcs["tcppointer"] = func() {
		ip := getCommandOrFailed("ip").(string)
		port := getCommandOrFailed("port").(string)
		portInt, _ := strconv.Atoi(port)
		fmt.Println(ip, portInt)
		client := tcpClient.TcpClient{}
		client.OnConnect(func(connection net.Conn) {
			connection.Write([]byte("xixxi"))
		})
		client.OnError(func(err error) {
			fmt.Println("err:", err)
		})
		client.OnMessage(func(connection net.Conn, message []byte) {
			fmt.Println("message:", string(message))
		})
		client.Connect(ip, portInt, true)
		time.Sleep(time.Second * 60)
	}
	if fun, ok := funcs[command]; !ok {
		for f, _ := range funcs {
			fmt.Println("func-->:" + f)
		}
	} else {
		fun()
	}

}

//func uploadTest() error {
//	requestJson := `{
//  "files": [
//    {
//      "path": "D:/HomeNasData/user_data/10000/albums/1721202937474_IMG_20240713_2103441721202886.jpg",
//      "source": "云桌面",
//      "origin_name": "IMG_20240713_2103441721202886.jpg",
//      "file_modify_timestamp": 1721202920
//    }
//  ]
//}`
//	req, err := http.NewRequest("POST", "https://172-19-176-1-4fbubg6beuts.direct.playnas.cool:20050/papi/picture/gallery/add", strings.NewReader(requestJson))
//	if err != nil {
//		return err
//	}
//	req.Header.Set("Content-Type", "application/json")
//	req.Header.Set("Cookie", "nas_token=bd330077-9603-4d1c-ad63-f747aed96b34; token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9wbGF5bmFzLmNvbVwvYXBpXC90b2tlblwvYWNjZXNzIiwiaWF0IjoxNzIxMjAyNzg4LCJleHAiOjE3MjM3OTQ3ODgsIm5iZiI6MTcyMTIwMjc4OCwianRpIjoiT3FSMm5vc3BlM0Y4bldNQiIsInN1YiI6MTksInBydiI6ImE0YzQ4OGE5MDcwZDMwNTFlYzgyZWFiYzliYTZjZGYyMWVkNjU1M2MiLCJsb2dpbl9kZXZpY2VfbmFtZSI6IntcInVhXCI6XCJNb3ppbGxhXC81LjAgKFdpbmRvd3MgTlQgMTAuMDsgV2luNjQ7IHg2NCkgQXBwbGVXZWJLaXRcLzUzNy4zNiAoS0hUTUwsIGxpa2UgR2Vja28pIENocm9tZVwvMTAxLjAuNDk1MS42NCBTYWZhcmlcLzUzNy4zNlwiLFwiYnJvd3NlclwiOntcIm5hbWVcIjpcIkNocm9tZVwiLFwidmVyc2lvblwiOlwiMTAxLjAuNDk1MS42NFwiLFwibWFqb3JcIjpcIjEwMVwifSxcImVuZ2luZVwiOntcIm5hbWVcIjpcIkJsaW5rXCIsXCJ2ZXJzaW9uXCI6XCIxMDEuMC40OTUxLjY0XCJ9LFwib3NcIjp7XCJuYW1lXCI6XCJXaW5kb3dzXCIsXCJ2ZXJzaW9uXCI6XCIxMFwifSxcImRldmljZVwiOnt9LFwiY3B1XCI6e1wiYXJjaGl0ZWN0dXJlXCI6XCJhbWQ2NFwifX0iLCJpcCI6IjE4Mi4xNTAuNDYuMTcyIiwiaXBfYWRkcmVzcyI6Ilx1NGUyZFx1NTZmZCBcdTU2ZGJcdTVkZGQgXHU2MjEwXHU5MGZkIn0.yAmXs0Nrj5Cxxaid5tzrt2GKWX4HXaslgkxYqFwCNdI; tfstk=fV7SxIYmdYD7Fa5XK3F25rIsmDYBeTaZpXOds63r9ULJkxdBhQhRw3HxDKRB22F3x2KCBCPh4QJepi6GFBSzEpSf9tXIaHrkUi9CHtYE9HLdMqdHn9t3ZpJpd95d4Sza7_fk-eHQQozZHcufzTOKJ2FpDCRhnRDq-_fk-jxnagucZDZOwj7JJ9LAMCRpJBK-ySTvtB8KyH3-DKpD9epdp3HvHBOH9Bpp9qLJFARQTL17GsmBrYGg669jJVGMw3HNtDotXZOWVN1HhIRVlQtWBnP5uH_R36QPrn4jkefVf9sdB-oyeGO1hhS7C461EBCB9Tai1KQ5OZtl0PHXiiCPvaXSO2dlzN-BInNK-H8A7hIOFXVGMUddKhbUI4vR91bGbFaSIQsA1EsyfVJ1j-iIGh0BGdPbGDmUpSoZQypw7Z-JiQTbGSMWk3dDGdPbGDmH2IAqcSNjFEC..")
//	client := &http.Client{}
//	res, err := client.Do(req)
//	if err != nil {
//		return err
//	}
//	defer res.Body.Close()
//	bodyString, err := io.ReadAll(res.Body)
//	if err != nil {
//		return err
//	}
//	fmt.Println(string(bodyString))
//	return nil
//
//}
//
//func main() {
//	var waitGroup sync.WaitGroup
//	for i := 0; i < 20; i++ {
//		waitGroup.Add(1)
//		go func() {
//			defer waitGroup.Done()
//			err := uploadTest()
//			if err != nil {
//				fmt.Println(err)
//			}
//		}()
//	}
//	waitGroup.Wait()
//	// 创建一个 TCP 服务器，监听 8080 端口
//	listener, err := net.Listen("tcp", ":7000")
//	if err != nil {
//		fmt.Println("Error listening:", err)
//		os.Exit(1)
//	}
//	defer listener.Close()
//
//	fmt.Println("Server is listening...")
//
//	for {
//		// 等待客户端连接
//		conn, err := listener.Accept()
//		if err != nil {
//			fmt.Println("Error accepting:", err)
//			os.Exit(1)
//		}
//		fmt.Println("accepted new connection..." + conn.RemoteAddr().String())
//
//		// 创建一个 goroutine 来处理连接
//		go handleRequest(conn)
//	}
//}

// 处理请求
func handleRequest(conn net.Conn) {
	// 关闭连接时，也关闭此 goroutine
	defer conn.Close()
	for true {
		// 向客户端发送一条消息
		buffer := make([]byte, 1024)
		n, err := conn.Read(buffer)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println(string(buffer[0:n]), "recv")
	}

}

package echo

import (
	"errors"
	"fmt"
	"os"
	"regexp"
)

const (
	textBlack = iota + 30
	textRed
	textGreen
	textYellow
	textBlue
	textPurple
	textCyan
	textWhite
)

func GetCmdParams() []string {
	return os.Args
}
func GetCmdInputParams() map[string]string {
	params := GetCmdParams()
	res := map[string]string{}
	for _, param := range params {
		reg, _ := regexp.Compile("^--(.*?)=(.*?)$")
		match := reg.FindStringSubmatch(param)
		if len(match) == 3 {
			res[match[1]] = match[2]
		}
	}
	return res
}
func GetCmdInputParamByKey(key string) (string, error) {
	params := GetCmdInputParams()
	if _, ok := params[key]; ok {
		return params[key], nil
	}
	return "", errors.New("commandParams not exist:" + key)
}
func CheckCmdParams(index int, fun func(value string) bool) bool {
	params := GetCmdParams()
	if len(params) >= index+1 {
		return fun(params[index])
	} else {
		return fun("")
	}
}
func Black(str string) {
	textColor(textBlack, str)
}

func Red(str string) {
	textColor(textRed, str)
}
func Yellow(str string) {
	textColor(textYellow, str)
}
func Green(str string) {
	textColor(textGreen, str)
}
func Cyan(str string) {
	textColor(textCyan, str)
}
func Blue(str string) {
	textColor(textBlue, str)
}
func Purple(str string) {
	textColor(textPurple, str)
}
func White(str string) {
	textColor(textWhite, str)
}

func textColor(color int, str string) {
	fmt.Println(fmt.Sprintf("\x1b[0;%dm%s\x1b[0m", color, str))
}

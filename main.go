package main

import (
	"fmt"
	"gitee.com/jjawesomejj/awesome-util/helper/commonhelper"
	"gitee.com/jjawesomejj/awesome-util/log"
	"gitee.com/jjawesomejj/awesome-util/tcpClient"
	"gitee.com/jjawesomejj/awesome-util/tcpserver"
	"gitee.com/jjawesomejj/awesome-util/test"
	"gitee.com/jjawesomejj/awesome-util/udp/udpServer"
	"net"
	"time"
)

func main() {
	port := 9985
	//ip := "127.0.0.1"
	//server := udpServer.UdpServer{
	//	ListenPort: port,
	//}
	//server.OnMessage(func(data []byte, udpCon *net.UDPConn, addr *net.UDPAddr) {
	//	fmt.Println(string(data))
	//})
	//go server.Start()
	//time.Sleep(time.Second * 1)
	//client := udpServer.UdpClient{
	//	IpAddress: ip,
	//	Port:      port,
	//}
	//_, err := client.Connect()
	//if err == nil {
	//	err := client.Send([]byte("赵李杰测试"))
	//	if err != nil {
	//		fmt.Println(err)
	//	}
	//}
	//time.Sleep(time.Second * 3)
	test.TestCase01()
	logger := log.FileLoger{
		LogPath: "./log/",
	}
	logger.Error("test")
	return
	values := []map[string]string{}
	values = append(values, map[string]string{
		"name": "123",
		"age":  "145",
	})
	commonhelper.PrintTable(values, true)
	server := tcpserver.TcpServer{}
	server.OnMessage(func(connection net.Conn, message []byte) {
		fmt.Println(message)
	})
	go server.Start(port, true)
	client := tcpClient.TcpClient{}
	client.OnConnect(func(connection net.Conn) {
		//fmt.Println("load")
		connection.Write([]byte("load"))
	})
	client.OnError(func(err error) {
		fmt.Println(err)
	})
	client.Connect("127.0.0.1", 9985, true)
	time.Sleep(time.Second * 20)
	udp := udpServer.UdpClient{
		Port:      9985,
		IpAddress: "127.0.0.1",
	}
	go udp.Connect()
	err := udp.Send([]byte("load"))
	if err != nil {
		return
	}
	time.Sleep(time.Second * 20)
}
